package com.studying.androidpartctask2;

import android.widget.EditText;

public final class BinaryValidator {
    private BinaryValidator() {
    }

    public static boolean validate(EditText binaryEditField) {
        String inStr = binaryEditField.getText().toString();
        String falseStr = inStr.replaceAll("1","").replaceAll("0","");
        if (falseStr.length() > 0){
            return false;
        }
        return true;
    }

    public static String validateString(String str) {
        String falseStr = str.replaceAll("1","").replaceAll("0","");
        return str.replaceAll(falseStr,"");
    }
}
