package com.studying.androidpartctask2;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public abstract class MyTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    protected void clearEditTextFields(EditText... fields) {
        for (EditText field : fields) {
            if (!field.getText().toString().isEmpty()) {
                field.setText("");
            }
        }
    }
}
