package com.studying.androidpartctask2;

import android.widget.EditText;

public final class HexValidator {
    private HexValidator() {
    }

    public static boolean validate(EditText hexEditField) {
        String inStr = hexEditField.getText().toString();
        String falseStr = inStr.replaceAll("0","")
                .replaceAll("1", "")
                .replaceAll("2", "")
                .replaceAll("3", "")
                .replaceAll("4","")
                .replaceAll("5","")
                .replaceAll("6","")
                .replaceAll("7","")
                .replaceAll("8","")
                .replaceAll("A","")
                .replaceAll("B","")
                .replaceAll("C","")
                .replaceAll("D","")
                .replaceAll("E","")
                .replaceAll("F","")
                .replaceAll("a","")
                .replaceAll("b","")
                .replaceAll("c","")
                .replaceAll("d","")
                .replaceAll("e","")
                .replaceAll("f","");
        if (falseStr.length() > 0){
            return false;
        }
        return true;
    }

    public static String validateString(String str) {
        String falseStr = str.replaceAll("0","")
                .replaceAll("1", "")
                .replaceAll("2", "")
                .replaceAll("3", "")
                .replaceAll("4","")
                .replaceAll("5","")
                .replaceAll("6","")
                .replaceAll("7","")
                .replaceAll("8","")
                .replaceAll("A","")
                .replaceAll("B","")
                .replaceAll("C","")
                .replaceAll("D","")
                .replaceAll("E","")
                .replaceAll("F","")
                .replaceAll("a","")
                .replaceAll("b","")
                .replaceAll("c","")
                .replaceAll("d","")
                .replaceAll("e","")
                .replaceAll("f","");
        return str.replaceAll(falseStr,"");
    }
}
