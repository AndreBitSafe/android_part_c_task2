package com.studying.androidpartctask2;

import android.widget.EditText;

public final class DecimalValidator {
    private DecimalValidator() {
    }

    public static boolean validate(EditText decimalEditField) {
        String inStr = decimalEditField.getText().toString();
        String falseStr = inStr.replaceAll("0","")
                .replaceAll("1", "")
                .replaceAll("2", "")
                .replaceAll("3", "")
                .replaceAll("4","")
                .replaceAll("5","")
                .replaceAll("6","")
                .replaceAll("7","")
                .replaceAll("8","")
                .replaceAll("9","");
        if (falseStr.length() > 0){
            return false;
        }
        return true;
    }

    public static String validateString(String str) {
        String falseStr = str.replaceAll("0","")
                .replaceAll("1", "")
                .replaceAll("2", "")
                .replaceAll("3", "")
                .replaceAll("4","")
                .replaceAll("5","")
                .replaceAll("6","")
                .replaceAll("7","")
                .replaceAll("8","")
                .replaceAll("9","");
        return str.replaceAll(falseStr,"");
    }
}
