package com.studying.androidpartctask2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText decimalEditField;
    private EditText binaryEditField;
    private EditText hexEditField;

    private TextView binaryTextField;
    private TextView octalTextField;
    private TextView decimalTextField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        decimalEditField = findViewById(R.id.decimal_edit_text_field);
        binaryEditField = findViewById(R.id.binary_edit_text_field);
        hexEditField = findViewById(R.id.hexadecimal_edit_text_field);

        binaryTextField = findViewById(R.id.binary_text_field);
        octalTextField = findViewById(R.id.octal_text_field);
        decimalTextField = findViewById(R.id.decimal_text_field);

        decimalEditField.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
                clearEditTextFields(binaryEditField, hexEditField);
            }

            int strLength = 0;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int inLength = decimalEditField.getText().toString().length();
                if (!DecimalValidator.validate(decimalEditField)) {
                    if (inLength == 1){
                        decimalEditField.setText("");
                        return;
                    }
                    String str = decimalEditField.getText().toString();
                    decimalEditField.setText(DecimalValidator.validateString(str));
                }
                if (inLength != strLength && inLength > 0) {
                    strLength = inLength;
                    long init = Long.parseLong(decimalEditField.getText().toString());
                    binaryTextField.setText(Long.toBinaryString(init));
                    octalTextField.setText(Long.toOctalString(init));
                    decimalTextField.setText(Long.toString(init));
                }
            }
        });

        binaryEditField.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
                clearEditTextFields(decimalEditField, hexEditField);
            }

            int strLength = 0;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int inLength = binaryEditField.getText().toString().length();
                if (!BinaryValidator.validate(binaryEditField)) {
                    if (inLength == 1){
                        binaryEditField.setText("");
                        return;
                    }
                    String str = binaryEditField.getText().toString();
                    binaryEditField.setText(BinaryValidator.validateString(str));
                }
                if (inLength != strLength && inLength > 0) {
                    strLength = inLength;
                    long init = Long.parseLong(binaryEditField.getText().toString(), 2);
                    binaryTextField.setText(Long.toBinaryString(init));
                    octalTextField.setText(Long.toOctalString(init));
                    decimalTextField.setText(Long.toString(init));
                }
            }
        });

        hexEditField.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
                clearEditTextFields(decimalEditField, binaryEditField);
            }

            int strLength = 0;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int inLength = hexEditField.getText().toString().length();
                if (!HexValidator.validate(hexEditField)) {
                    if (inLength == 1){
                        hexEditField.setText("");
                        return;
                    }
                    String str = hexEditField.getText().toString();
                    hexEditField.setText(HexValidator.validateString(str));
                }
                if (inLength != strLength && inLength > 0) {
                    strLength = inLength;
                    long init = Long.parseLong(hexEditField.getText().toString(), 16);
                    binaryTextField.setText(Long.toBinaryString(init));
                    octalTextField.setText(Long.toOctalString(init));
                    decimalTextField.setText(Long.toString(init));
                }
            }
        });
    }

}
